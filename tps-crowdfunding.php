<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://theparentsshop.com
 * @since             1.0.0
 * @package           Tps_Crowdfunding
 *
 * @wordpress-plugin
 * Plugin Name:       The Parents Shop Crowdfunding
 * Plugin URI:        http://theparentsshop.com
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           2.0.0
 * Author:            Vasilis Koutsopoulos
 * Author URI:        http://theparentsshop.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       tps-crowdfunding
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-tps-crowdfunding-activator.php
 */
function activate_tps_crowdfunding() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-tps-crowdfunding-activator.php';
	Tps_Crowdfunding_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-tps-crowdfunding-deactivator.php
 */
function deactivate_tps_crowdfunding() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-tps-crowdfunding-deactivator.php';
	Tps_Crowdfunding_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_tps_crowdfunding' );
register_deactivation_hook( __FILE__, 'deactivate_tps_crowdfunding' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-tps-crowdfunding.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_tps_crowdfunding() {

	$plugin = new Tps_Crowdfunding();
	$plugin->run();

}
run_tps_crowdfunding();
