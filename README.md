# How to use

In Product Edit page, in General Tab, set the Date and Time that the crowdfunding expires. If Time is empty the default value is 23:59.

# For developers

For displaying the remaining expiration time, we use the javascript library [jQuery.countdown](http://hilios.github.io/jQuery.countdown).