<?php
/**
 * Crowdfunding Expiry Meta
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Tps_Post_Meta_Crowdfunding_Expiry Class.
 */
class Tps_Post_Meta_Crowdfunding_Expiry {

   /**
	* The HTML for the crowdfunding date meta
	*
	*/
	static function render( $post ) {
		
		global $post;

		$timezone_string = get_option( 'timezone_string' , 'Europe/Athens' );

		$crowdfunding_expiry_date = '';
		$crowdfunding_expiry_time = '';
		
		$crowdfunding_expiry_timestamp = get_post_meta( $post->ID, '_tps_crowdfunding_expiry', true ) ;

		if ( !empty ( $crowdfunding_expiry_timestamp ) )
		{
			$crowdfunding_expiry = new DateTime();

			$crowdfunding_expiry->setTimestamp( $crowdfunding_expiry_timestamp );

			$crowdfunding_expiry->setTimezone( new DateTimeZone( $timezone_string ) );

			$crowdfunding_expiry_date = $crowdfunding_expiry -> format('Y/m/d') ;
			$crowdfunding_expiry_time = $crowdfunding_expiry -> format('H:i') ;
		}

		wp_nonce_field( basename( __FILE__ ), '_tps_crowdfunding_expiry_nonce' );

	?>

        <div class="crowdfunding_expiry show_if_external" style="border-top: 1px solid #eee;">

            <p><?php _e( 'Crowdfunding Expiry', 'tps-crowdfunding' ) ;?></p>

            <p class="form-field _tps_crowdfunding_expiry_date">

                <label for="_tps_crowdfunding_expiry_date"><?php _e( 'Date (YYYY/MM/DD)', 'tps-crowdfunding' ) ;?></label>

                <input type="text" class="short" name="_tps_crowdfunding_expiry_date" id="_tps_crowdfunding_expiry_date"
                    value="<?php echo esc_attr( $crowdfunding_expiry_date ) ;?>"
                    placeholder="YYYY/MM/DD"
                    maxlength="10"
                    pattern="[0-9]{4}/(0[1-9]|1[012])/(0[1-9]|1[0-9]|2[0-9]|3[01])" />

                <a href="javascript:void(0);" class="crowdfunding_expiry_date" onclick="jQuery('#_tps_crowdfunding_expiry_date,#_tps_crowdfunding_expiry_time').val('');"><?php _e( 'Clear', 'tps-crowdfunding' ) ;?></a>

            </p>

            <p class="form-field _tps_crowdfunding_expiry_time">

                <label for="_tps_crowdfunding_expiry_time"><?php _e( 'Time (HH:MM)', 'tps-crowdfunding' ) ;?></label>

                <input type="text" class="short" name="_tps_crowdfunding_expiry_time" id="_tps_crowdfunding_expiry_time"
                    value="<?php echo esc_attr( $crowdfunding_expiry_time ) ;?>"
                    placeholder="HH:MM"
                    maxlength="5"
                    pattern="(00|[0-9]|1[0-9]|2[0-3]):([0-9]|[0-5][0-9])" />

                <span class="description"><?php _e( 'Defaults to 23:59 if empty', 'tps-crowdfunding' ) ;?></span>

            </p>

            <p class="description"><?php _e( 'Timezone: ', 'tps-crowdfunding' ) ;?>  <?php echo $timezone_string ;?> </p>

        </div>

	

	<?php 

	}

   /**
	* Save _tps_currency post meta
	*
	*/
	static function save( $post_id ) {

		global $post;
		
		// Verify nonce
		if ( !isset( $_POST['_tps_crowdfunding_expiry_nonce'] ) || !wp_verify_nonce( $_POST['_tps_crowdfunding_expiry_nonce'], basename(__FILE__) ) ) {
			return $post_id;
		}
		
		// Check Autosave
		if ( (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) || ( defined('DOING_AJAX') && DOING_AJAX) || isset($_REQUEST['bulk_edit']) ) {
			return $post_id;
		}

		// Don't save if only a revision
		if ( isset( $post->post_type ) && $post->post_type == 'revision' ) {
			return $post_id;
		}

		// Check permissions
		if ( !current_user_can( 'edit_product', $post->ID ) ) {
			return $post_id;
		}

		$timezone_string = get_option( 'timezone_string' , 'Europe/Athens' );


		$crowdfunding_expiry_date = $_POST['_tps_crowdfunding_expiry_date'];

		// If date is empty delete post meta
		if ( empty ( $crowdfunding_expiry_date ) )
		{
			 delete_post_meta( $post->ID, '_tps_crowdfunding_expiry' );

			 return $post_id;
		}

		$crowdfunding_expiry_time = $_POST['_tps_crowdfunding_expiry_time'];

		if ( empty ( $crowdfunding_expiry_time ) )
		{
			 $crowdfunding_expiry_time = '23:59';
		}

		$crowdfunding_expiry = new DateTime( $crowdfunding_expiry_date.' '.$crowdfunding_expiry_time , new DateTimeZone( $timezone_string ) );

		$crowdfunding_expiry_timestamp = $crowdfunding_expiry->getTimestamp(); ;

		update_post_meta( $post->ID, '_tps_crowdfunding_expiry', $crowdfunding_expiry_timestamp );
	}

}