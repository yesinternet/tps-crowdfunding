<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://theparentsshop.com
 * @since      1.0.0
 *
 * @package    Tps_Crowdfunding
 * @subpackage Tps_Crowdfunding/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Tps_Crowdfunding
 * @subpackage Tps_Crowdfunding/includes
 * @author     Vasilis Koutsopoulos <vaskou@theparentsshop.com>
 */
class Tps_Crowdfunding_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'tps-crowdfunding',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
