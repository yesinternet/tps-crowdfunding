<?php

/**
 * Fired during plugin activation
 *
 * @link       http://theparentsshop.com
 * @since      1.0.0
 *
 * @package    Tps_Crowdfunding
 * @subpackage Tps_Crowdfunding/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Tps_Crowdfunding
 * @subpackage Tps_Crowdfunding/includes
 * @author     Vasilis Koutsopoulos <vaskou@theparentsshop.com>
 */
class Tps_Crowdfunding_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
