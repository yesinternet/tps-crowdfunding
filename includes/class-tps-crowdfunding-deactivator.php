<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://theparentsshop.com
 * @since      1.0.0
 *
 * @package    Tps_Crowdfunding
 * @subpackage Tps_Crowdfunding/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Tps_Crowdfunding
 * @subpackage Tps_Crowdfunding/includes
 * @author     Vasilis Koutsopoulos <vaskou@theparentsshop.com>
 */
class Tps_Crowdfunding_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
