<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://theparentsshop.com
 * @since      1.0.0
 *
 * @package    Tps_Crowdfunding
 * @subpackage Tps_Crowdfunding/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Tps_Crowdfunding
 * @subpackage Tps_Crowdfunding/public
 * @author     Vasilis Koutsopoulos <vaskou@theparentsshop.com>
 */
class Tps_Crowdfunding_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Tps_Crowdfunding_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Tps_Crowdfunding_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

//		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/tps-crowdfunding-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Tps_Crowdfunding_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Tps_Crowdfunding_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		//wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/tps-crowdfunding-public.js', array( 'jquery' ), $this->version, true );
		wp_register_script( $this->plugin_name . '-jquery-countdown', plugin_dir_url( __FILE__ ) . 'js/jquery.countdown.min.js', array( 'jquery' ), $this->version, true );
		wp_register_script( $this->plugin_name . '-tps-countdown', plugin_dir_url( __FILE__ ) . 'js/tps-countdown.js', array( 'jquery' ), $this->version, true );


	}

    /**
     * Function that returns the remaining time that the crowdfunding has to end
     * @return string
     */
    public function show_countdown() {

        global $product;

        if(!$product->is_type('external')){
            return '';
        }

        $post_id = $product->get_id();

        $product_crowdfunding_expiry = get_post_meta($post_id, '_tps_crowdfunding_expiry', true);

        if(empty($product_crowdfunding_expiry)){
            return '';
        }

	    $return = '<div class="tps-countdown" data-countdown="' . $product_crowdfunding_expiry .'"></div>';

        wp_enqueue_script( $this->plugin_name . '-jquery-countdown' );
        wp_enqueue_script( $this->plugin_name . '-tps-countdown' );

        return $return;

	}

    /**
     * Function that displays an icon and 'Crowdfunding' text, that can be added to template with an action
     */
    public function show_countdown_icon() {
        $countdown = $this->show_countdown();

        if(empty($countdown)) return;

        $title = __( 'Crowdfunding', 'tps-crowdfunding' );

        echo '<span class="btn tps-btn tps-btn-crowdfunding" data-toggle="tooltip" data-placement="bottom" title="' . $title . '">';
        echo '    <i class="fa fa-clock-o" aria-hidden="true"></i> ';
        echo '    <span>' . $title . '</span>';
        echo '</span>';
	}

    /**
     * Function that displays the remaining time that the crowdfunding has to end,
     * and is used for 'woocommerce_after_shop_loop_item_title' action
     */
    public function show_countdown_product_grid(){
        $countdown = $this->show_countdown();

        if(empty($countdown)) return;

        echo '<div class="tps-product-expiry">';
        echo    $countdown;
        echo '</div>';
    }

    /**
     * Function that displays the remaining time that the crowdfunding has to end,
     * and is used for 'woocommerce_single_product_summary' action
     */
	public function show_countdown_product_page(){
        $this->show_countdown_product_grid();
    }

}
