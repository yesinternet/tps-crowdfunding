(function( $ ) {
    $("[data-countdown]").each(function(){
        var $this = $(this);
        var timestamp = $this.data("countdown") * 1000;

        $this.countdown(timestamp)
            .on('update.countdown', function(event){
                var format = '%H:%M:%S';

                if(event.offset.totalDays > 0) {
                    format = '%-D day%!D ';
                }

                if(event.offset.months > 0) {
                    format = '%-m month%!m ';
                }

                $this.html('<span>' + event.strftime(format) + ' left</span>');
            })
            .on('finish.countdown', function(event) {
                $this.html('<span class="countdown-finished">Crowdfunding period ended</span>');
            });
    });
})( jQuery );